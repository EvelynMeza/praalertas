package com.example.practicaalertas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.practicaalertas.Api.Api;
import com.example.practicaalertas.Api.Servicio.ServicioPeticion;
import com.example.practicaalertas.ViewModels.Peticion_Crear;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Notifi extends AppCompatActivity {
    private Button btn14,btn15;
    private EditText Texto7;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifi);
        btn14=(Button)findViewById(R.id.button14);
        btn15=(Button)findViewById(R.id.button15);
        Texto7=(EditText) findViewById(R.id.editText7);
        btn15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
                startActivity(intent);
            }
        });

        btn14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
                ServicioPeticion s = Api.getApi(Notifi.this).create(ServicioPeticion.class);
                String id = preferencias.getString("ID","");
                int ids= Integer.parseInt(id.toString());
                int ida= Integer.parseInt(Texto7.getText().toString());
                Call<Peticion_Crear> Registros = s.getVisA(ids,ida);
                Registros.enqueue(new Callback<Peticion_Crear>() {
                    @Override
                    public void onResponse(Call<Peticion_Crear> call, Response<Peticion_Crear> response) {
                        Peticion_Crear mostrar = response.body();
                        String Cadena = "";
                        if (mostrar.estado.equals("true")){

                            Toast.makeText(Notifi.this, "Se creo Correctamente", Toast.LENGTH_SHORT).show();

                        }else{
                            Toast.makeText(Notifi.this, "Datos Incorrectos", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Peticion_Crear> call, Throwable t) {
                        Toast.makeText(Notifi.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

    }
}
