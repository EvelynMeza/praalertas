package com.example.practicaalertas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.practicaalertas.Api.Api;
import com.example.practicaalertas.Api.Servicio.ServicioPeticion;
import com.example.practicaalertas.ViewModels.AlertasUs;
import com.example.practicaalertas.ViewModels.Peticion_Alertas;
import com.example.practicaalertas.ViewModels.Peticion_Crear;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuActivity extends AppCompatActivity {
    private Button btn5,btn6,btn7,btn8,btn9,btn16;
    private EditText texto;
    public String APITOKEN;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        btn5=(Button)findViewById(R.id.button5);
        btn6=(Button)findViewById(R.id.button6);
        btn7=(Button)findViewById(R.id.button7);
        btn8=(Button)findViewById(R.id.button8);
        btn9=(Button)findViewById(R.id.button9);
        btn16=(Button)findViewById(R.id.button16);
        texto=(EditText)findViewById(R.id.editText6);
        FirebaseMessaging.getInstance().subscribeToTopic("atodos").addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(MenuActivity.this, "Se agrego a la lista", Toast.LENGTH_SHORT).show();
            }
        });

        btn16.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            llamarGeneral();
            }
        });

        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
                ServicioPeticion s = Api.getApi(MenuActivity.this).create(ServicioPeticion.class);
                String id = preferencias.getString("ID","");
                int ids= Integer.parseInt(id.toString());
                Call<Peticion_Crear> Registros = s.getAlert(ids);
                Registros.enqueue(new Callback<Peticion_Crear>() {
                    @Override
                    public void onResponse(Call<Peticion_Crear> call, Response<Peticion_Crear> response) {
                        Peticion_Crear mostrar = response.body();
                        String Cadena = "";
                        if (mostrar.estado.equals("true")){

                            Toast.makeText(MenuActivity.this, "Se creo Correctamente", Toast.LENGTH_SHORT).show();

                        }else{
                            Toast.makeText(MenuActivity.this, "Datos Incorrectos", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Peticion_Crear> call, Throwable t) {
                        Toast.makeText(MenuActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                texto.setText("");
                SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
                ServicioPeticion s = Api.getApi(MenuActivity.this).create(ServicioPeticion.class);
                String id = preferencias.getString("ID", "");
                int ids= Integer.parseInt(id.toString());
                Call<AlertasUs> Registros = s.getAlertU(ids);
                Registros.enqueue(new Callback<AlertasUs>() {
                    @Override
                    public void onResponse(Call<AlertasUs> call, Response<AlertasUs> response) {
                        AlertasUs mostrar = response.body();
                        String Cadena = "";
                        if (mostrar.estado.equals("true")){

                            for (dt elemento: mostrar.alertas
                            ) {
                                Cadena = Cadena + elemento.id + " | " + elemento.usuarioId + " | " + elemento.created_at + " | " + elemento.updated_at + " \n ";
                                texto.setText(Cadena);
                            }

                        }else{
                            Toast.makeText(MenuActivity.this, "Datos Incorrectos", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<AlertasUs> call, Throwable t) {
                        Toast.makeText(MenuActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ServicioPeticion s = Api.getApi(MenuActivity.this).create(ServicioPeticion.class);

                Call<Peticion_Alertas> Registros = s.getAlertas();
                Registros.enqueue(new Callback<Peticion_Alertas>() {
                    @Override
                    public void onResponse(Call<Peticion_Alertas> call, Response<Peticion_Alertas> response) {
                        Peticion_Alertas mostrar = response.body();
                        String Cadena = "";
                        if (!mostrar.alertas.isEmpty()){

                            for (dt elemento: mostrar.alertas
                            ) {
                                Cadena = Cadena + elemento.id + " | " + elemento.usuarioId + " | " + elemento.created_at + " | " + elemento.updated_at + " \n ";
                                texto.setText(Cadena);
                            }

                        }else{
                            Toast.makeText(MenuActivity.this, "Datos Incorrectos", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Peticion_Alertas> call, Throwable t) {
                        Toast.makeText(MenuActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferencias = getSharedPreferences("credenciales",Context.MODE_PRIVATE);
                String tokenn = APITOKEN;
                SharedPreferences.Editor editor = preferencias.edit();
                editor.putString("TOKEN","");
                editor.commit();
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });

        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Main2Activity.class);
                startActivity(intent);
            }
        });
    }
    private void llamarGeneral(){
        RequestQueue myrequest = Volley.newRequestQueue(getApplicationContext());
        JSONObject json = new JSONObject();
        try {
            json.put("to","/topics/"+"atodos");
            JSONObject notificaciones = new JSONObject();
            notificaciones.put("titulo", "Hola");
            notificaciones.put("detalle","Vato");
            json.put("data",notificaciones);
            String URL = "https://fcm.googleapis.com/fcm/send";
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,URL,json,null, null){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> header = new HashMap<>();
                    header.put("content-type","application/json");
                    header.put("authorization","key=AAAA6vEPH_k:APA91bHxor8vsz7-WZHtTl-ab1oJodDvRUiyJgnSCa20wuwxxQa9x2YRDlzTDqrLIE9pGcu6sZRuuopcwQ6DdSvsgzZwJYuZbWYV0SiJ5FvSbyBX9EhcQoAJ7nvQvCf2MJMAKracL-xE");
                    return header;
                }
            };
            myrequest.add(request);
        }catch (JSONException e){
            e.printStackTrace();
        }
    }
}
