package com.example.practicaalertas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.practicaalertas.Api.Api;
import com.example.practicaalertas.Api.Servicio.ServicioPeticion;
import com.example.practicaalertas.ViewModels.Peticion_VisU;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Main2Activity extends AppCompatActivity {
    private Button  btn10,btn11,btn12,btn13;
    private EditText texto2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        btn10=(Button)findViewById(R.id.button10);
        btn11=(Button)findViewById(R.id.button11);
        btn12=(Button)findViewById(R.id.button12);
        btn13=(Button)findViewById(R.id.button13);
        texto2=(EditText) findViewById(R.id.editText5);

        btn10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
                ServicioPeticion s = Api.getApi(Main2Activity.this).create(ServicioPeticion.class);
                String id = preferencias.getString("ID", "");
                int ids= Integer.parseInt(id.toString());
                Call<Peticion_VisU> Registros = s.getVisU(ids);
                Registros.enqueue(new Callback<Peticion_VisU>() {
                    @Override
                    public void onResponse(Call<Peticion_VisU> call, Response<Peticion_VisU> response) {
                        Peticion_VisU mostrar = response.body();
                        String Cadena = "";
                        if (mostrar.estado.equals("true")){

                            for (dt elemento: mostrar.visualizaciones
                            ) {
                                Cadena = Cadena + elemento.id + " | " + elemento.usuarioId + " | "  + elemento.alertaId + " | "+ elemento.created_at + " | " + elemento.updated_at + " \n ";
                                texto2.setText(Cadena);
                            }

                        }else{
                            Toast.makeText(Main2Activity.this, "Datos Incorrectos", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Peticion_VisU> call, Throwable t) {
                        Toast.makeText(Main2Activity.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        btn11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ServicioPeticion s = Api.getApi(Main2Activity.this).create(ServicioPeticion.class);

                Call<Peticion_VisU> Registros = s.getVis();
                Registros.enqueue(new Callback<Peticion_VisU>() {
                    @Override
                    public void onResponse(Call<Peticion_VisU> call, Response<Peticion_VisU> response) {
                        Peticion_VisU mostrar = response.body();
                        String Cadena = "";
                        if (!mostrar.visualizaciones.isEmpty()){

                            for (dt elemento: mostrar.visualizaciones
                            ) {
                                Cadena = Cadena + elemento.id + " | " + elemento.usuarioId + " | "  + elemento.alertaId + " | "+ elemento.created_at + " | " + elemento.updated_at + " \n ";
                                texto2.setText(Cadena);
                            }

                        }else{
                            Toast.makeText(Main2Activity.this, "Datos Incorrectos", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Peticion_VisU> call, Throwable t) {
                        Toast.makeText(Main2Activity.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        btn12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                texto2.setText("");
            }
        });

        btn13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
                startActivity(intent);
            }
        });
    }
}
