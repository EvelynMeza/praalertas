package com.example.practicaalertas.ViewModels;

import com.example.practicaalertas.dt;

import java.util.List;

public class Peticion_Alertas {
    public List<dt> alertas;

    public List<dt> getAlertas() {
        return alertas;
    }

    public void setAlertas(List<dt> alertas) {
        this.alertas = alertas;
    }
}
