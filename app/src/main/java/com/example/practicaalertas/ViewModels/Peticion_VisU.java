package com.example.practicaalertas.ViewModels;

import com.example.practicaalertas.dt;

import java.util.List;

public class Peticion_VisU {
public String estado;
public List<dt> visualizaciones;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<dt> getVisualizaciones() {
        return visualizaciones;
    }

    public void setVisualizaciones(List<dt> visualizaciones) {
        this.visualizaciones = visualizaciones;
    }
}
