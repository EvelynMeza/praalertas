package com.example.practicaalertas.ViewModels;

import com.example.practicaalertas.dt;

import java.util.List;

public class AlertasUs {
    public String estado;
    public List<dt> alertas;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<dt> getAlertas() {
        return alertas;
    }

    public void setAlertas(List<dt> alertas) {
        this.alertas = alertas;
    }
}
