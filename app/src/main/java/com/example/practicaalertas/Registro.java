package com.example.practicaalertas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.practicaalertas.Api.Api;
import com.example.practicaalertas.Api.Servicio.ServicioPeticion;
import com.example.practicaalertas.ViewModels.Registro_Usuario;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Registro extends AppCompatActivity {

    private Button inicio;
    private Button btn3;
    private EditText nu;
    private EditText contra;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        inicio=(Button)findViewById(R.id.button4);
        btn3=(Button)findViewById(R.id.button3);
        nu=(EditText)findViewById(R.id.editText3);
        contra=(EditText)findViewById(R.id.editText4);
        inicio.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);

            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ServicioPeticion service = Api.getApi(Registro.this).create(ServicioPeticion.class);
                Call<Registro_Usuario> registrarCall =  service.registrarUsuario(nu.getText().toString(),contra.getText().toString());
                registrarCall.enqueue(new Callback<Registro_Usuario>() {
                    @Override
                    public void onResponse(Call<Registro_Usuario> call, Response<Registro_Usuario> response) {
                        Registro_Usuario peticion = response.body();
                        if(response.body() == null){
                            Toast.makeText(Registro.this, "Ocurrio un Error, intentalo más tarde", Toast.LENGTH_LONG).show();
                            return;
                        }
                        if(peticion.estado == "true"){
                            startActivity(new Intent(Registro.this,MainActivity.class));
                            Toast.makeText(Registro.this, "Registro exitoso", Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(Registro.this, peticion.detalle, Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onFailure(Call<Registro_Usuario> call, Throwable t) {
                        Toast.makeText(Registro.this, "Error :( ", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
    }

