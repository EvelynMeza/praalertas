package com.example.practicaalertas.Api.Servicio;

import com.example.practicaalertas.ViewModels.AlertasUs;
import com.example.practicaalertas.ViewModels.Peticion_Alertas;
import com.example.practicaalertas.ViewModels.Peticion_Crear;
import com.example.practicaalertas.ViewModels.Peticion_Login;
import com.example.practicaalertas.ViewModels.Peticion_VisU;
import com.example.practicaalertas.ViewModels.Registro_Usuario;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ServicioPeticion {
    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<Registro_Usuario> registrarUsuario(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/login")
    Call<Peticion_Login> getLogin(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/crearAlerta")
    Call<Peticion_Crear> getAlert (@Field("usuarioId") int id) ;

    @FormUrlEncoded
    @POST("api/alertasusuario")
    Call<AlertasUs> getAlertU(@Field("usuarioId") int id );

    @POST("api/alertas")
    Call<Peticion_Alertas> getAlertas();

    @FormUrlEncoded
    @POST("api/crearvisualizacionalerta")
    Call<Peticion_Crear> getVisA(@Field("usuarioId") int id, @Field("alertaId") int idAlert);

    @FormUrlEncoded
    @POST("api/visualizacionesusuario")
    Call<Peticion_VisU> getVisU(@Field("usuarioId") int id);

    @POST("api/visualizaciones")
        Call<Peticion_VisU> getVis();


}
